// Import des modules
const express = require("express"); //Framework basé sur node.js
const mongoose = require("mongoose"); // Database MongoDB
const path = require("path"); // Upload des images + répertoir et chemin
const helmet = require('helmet'); // Protection
const nocache = require('nocache'); // Désactive le cache client
const bodyParser = require("body-parser"); // Extrait JSON des POST
const cors = require("cors")


// Masque les infos de connexion à la BDD
require('dotenv').config();

//Création d'application express
const app = express(); // Framework Express

//middleware
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(helmet({crossOriginResourcePolicy: {policy:"same-site"}}));
app.use(nocache());
app.use('/images', express.static(path.join(__dirname, 'images')));
app.use(cors({
    origin: "*"
}))

//Import des routes necessaire
const userRoutes = require("./route/user");
const sauceRoutes = require("./route/sauce");



//Connection à la bdd MongoDB
mongoose
    .connect(
        "mongodb+srv://UserNovem2:UserNovem2@ocrp6.vlj4e2w.mongodb.net/?retryWrites=true&w=majority", {
            useNewUrlParser: true,
            useUnifiedTopology: true
        }
    )
    .then(() => console.log("Connexion à MongoDB réussie !"))
    .catch(() => console.log("Connexion à MongoDB échouée !"));

//Header pour les erreurs de CORS
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Cross-Origin-Resource-Policy', 'cross-origin');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    // res.setHeader('Cross-Origin-Embedder-Policy', 'unsafe-none');
    // res.setHeader('Content-Security-Policy', "default-src 'self'");
    next();
});

//Routes attendues
app.use("/api/auth", userRoutes);
app.use("/api/sauces", sauceRoutes);



module.exports = app;