const Sauce = require("../model/sauce"); // Modèle ' sauce '
const fs = require("fs"); // File System de node -> dl et modif d'image

// création d'une sauce par un User
exports.createSauce = (req, res, next) => {
  const sauceObject = JSON.parse(req.body.sauce);
  delete sauceObject._id;
  const sauce = new Sauce({
    ...sauceObject,
    likes: 0,
    dislikes: 0,
    usersDisliked: [],
    usersLiked: [],
    imageUrl: `${req.protocol}://${req.get("host")}/images/${
      req.file.filename
    }`,
  });
  sauce
    .save()
    .then(() => res.status(201).json({
      message: "Sauce enregistré"
    }))
    .catch((error) => {
      console.log(error);
      res.status(400).json({
        message: "error"
      })
    })
};

//User cherche UNE sauce
exports.getOneSauce = (req, res, next) => {
  Sauce.findOne({
      _id: req.params.id,
    })
    .then((sauce) => {
      res.status(200).json(sauce);
    })
    .catch((error) => {
      res.status(404).json({
        error: error,
      });
    });
};

// User update sa sauce
exports.modifySauce = (req, res, next) => {
  Sauce.findOne({
    _id: req.params.id
  }).then((sauce) => {
    const filename = sauce.imageUrl.split("/images/")[1];

    let updatedSauce = {}
    if (req.file) {
      updatedSauce = {
        ...JSON.parse(req.body.sauce),
        imageUrl: `${req.protocol}://${req.get("host")}/images/${
      req.file.filename
    }`
      }
      fs.unlink(`images/${filename}`, () => null)

    } else {
      updatedSauce = {
        ...req.body
      }
    }
    Sauce.updateOne({
        _id: req.params.id
      }, {
        ...updatedSauce,
      })
      .then(() => res.status(200).json({
        message: "Sauce modifiée"
      }))
      .catch((error) => res.status(400).json({
        error
      }));
  });
};

// User supprime une sa sauce
exports.deleteSauce = (req, res, next) => {
  Sauce.findOne({
      _id: req.params.id
    })
    .then((sauce) => {
      const filename = sauce.imageUrl.split("/images/")[1];
      fs.unlink(`images/${filename}`, () => {
        Sauce.deleteOne({
            _id: req.params.id
          })
          .then(() => res.status(200).json({
            message: "Sauce supprimé"
          }))
          .catch((error) => res.status(400).json({
            error
          }));
      });
    })
    .catch((error) => res.status(500).json({
      error
    }));
};

// User cherche toutes les sauces
exports.getAllSauces = (req, res, next) => {
  Sauce.find()
    .then((sauces) => {
      res.status(200).json(sauces);
    })
    .catch((error) => {
      res.status(400).json({
        error: error,
      });
    });
};

// User Like ou Dislike une sauce
exports.likeSauce = (req, res, next) => {
  const sauceId = req.params.id;
  const userId = req.body.userId;
  const like = req.body.like;

  // User likes une sauce (like === 1)
  if (like === 1) {
    Sauce.updateOne({
        _id: sauceId
      }, {
        $inc: {
          likes: like
        },
        $push: {
          usersLiked: userId
        },
      })
      .then((sauce) => res.status(200).json({
        message: "Sauce Liké"
      }))
      .catch((error) => res.status(500).json({
        error
      }));
  }

  // User Dislikes une sauce (like === -1)
  else if (like === -1) {
    Sauce.updateOne({
        _id: sauceId
      }, {
        $inc: {
          dislikes: -1 * like
        },
        $push: {
          usersDisliked: userId
        },
      })
      .then((sauce) => res.status(200).json({
        message: "Sauce Disliké"
      }))
      .catch((error) => res.status(500).json({
        error
      }));
  }
  // User change d'avis sur une sauce qu'il aime
  else {
    Sauce.findOne({
        _id: sauceId
      })
      .then((sauce) => {
        if (sauce.usersLiked.includes(userId)) {
          Sauce.updateOne({
              _id: sauceId
            }, {
              $pull: {
                usersLiked: userId
              },
              $inc: {
                likes: -1
              }
            })
            .then((sauce) => {
              res.status(200).json({
                message: "Sauce dépréciée"
              });
            })
            .catch((error) => res.status(500).json({
              error
            }));

          // User change d'avis sur une sauce qu'il n'aime pas
        } else if (sauce.usersDisliked.includes(userId)) {
          Sauce.updateOne({
              _id: sauceId
            }, {
              $pull: {
                usersDisliked: userId
              },
              $inc: {
                dislikes: -1
              },
            })
            .then((sauce) => {
              res.status(200).json({
                message: "Sauce Liké"
              });
            })
            .catch((error) => res.status(500).json({
              error
            }));
        }
      })
      .catch((error) => res.status(401).json({
        error
      }));
  }
};