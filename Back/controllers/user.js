const bcrypt = require("bcrypt"); // hash le psw
const jwt = require("jsonwebtoken"); // token pour le User quand il se connecte
const validator = require("email-validator"); // Valide les email
const User = require("../model/user"); // Modèle User

// Controller pour l'inscription
exports.signup = (req, res, next) => {
  const isValidateEmail = validator.validate(req.body.email);
  if (!isValidateEmail) {
    res.writeHead(400, 'Email invalide"}', {
      "content-type": "application/json",
    });
    res.end("L'email est incorrect.");
  } else {
    bcrypt
      .hash(req.body.password, 10)
      .then(hash => {
        const user = new User({
          email: req.body.email,
          password: hash,
        });
        user.save()
          .then(() => res.status(201).json({
            message: "Utilisateur créé"
          }))
          .catch((error) => res.status(400).json({
            error
          }));
      })
      .catch((error) => res.status(500).json({
        error
      }));
  }
};


// Controller pour la connexion
exports.login = (req, res, next) => {
  User.findOne({
      email: req.body.email
    })
    .then((user) => {
      if (!user) {
        return res.status(401).json({
          error: "Utilisateur ou Mot de passe incorrect"
        });
      }
      bcrypt.compare(req.body.password, user.password)
        .then((valid) => {
          if (!valid) {
            return res.status(401).json({
              error: "Utilisateur ou Mot de passe incorrect"
            });
          }
          res.status(200).json({
            userId: user._id,
            token: jwt.sign({
              userId: user._id
            }, 'privatekey', {
              expiresIn: "24h",
            }),
          });
        })
        .catch((error) => res.status(500).json({
          error
        }));
    })
    .catch((error) => res.status(500).json({
      error
    }));
};