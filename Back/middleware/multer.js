const multer = require("multer");

//middleware multer pour la gestion des images
const types = {
  "image/jpg": "jpg",
  "image/jpeg": "jpg",
  "image/png": "png",
};

const storage = multer.diskStorage({
  destination: (req, file, callback) => {
    callback(null, "images");
  },
  filename: (req, file, callback) => {
    let name = file.originalname.split(" ").join("_");
    let extension = types[file.mimetype];
    let nameWithoutExtension = name.replace(`.${extension}`, "")
    callback(null, nameWithoutExtension + Date.now() + "." + extension);
  },
});

module.exports = multer({ storage: storage }).single("image");