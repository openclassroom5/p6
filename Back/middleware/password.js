// Schema du mdp
const passwordSchema = require("../model/password");

// Verif mdp schema et mdp du User.
module.exports = (req, res, next) => {
  if (!passwordSchema.validate(req.body.password)) {
    res.writeHead(
      400,
      "Le mot de passe doit comprendre 5 caractères dont un chiffre, sans espaces",
      {
        "content-type": "application/json",
      }
    );
    res.end("Format du mot de passe est incorrect.");
  } else {
    next();
  }
};