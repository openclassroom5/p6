const passwordValidator = require("password-validator");

// Crée un schema
const passwordSchema = new passwordValidator();

// Ajoute les propriétées du schéma
passwordSchema
  .is()
  .min(5) // Min 5
  .is()
  .max(50) // Max 50
  .has()
  .uppercase() // Doit avoir des lettres MAJUSCULES
  .has()
  .lowercase() // Doit avoir des lettres MINUSCULES
  .has()
  .digits(1) // Doit avoir min UN chiffre
  .has()
  .not()
  .spaces() // Ne doit pas avoir d'espace
  .is()
  .not()
  .oneOf(["Passw0rd", "Password123", "P4ssword", "Password000"]); // Blacklist

module.exports = passwordSchema;