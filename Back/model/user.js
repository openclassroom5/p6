// Mongoose + Validateur
const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator"); // Valide l'email comme unique


//Model des users
const userSchema = mongoose.Schema({

  //L'email doit être unique
  email: {
    type: String,
    required: [true, "Veuillez entrer votre adresse email"],
    unique: true,
    match: [/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/, "Veuillez entrer une adresse email correcte"]
  },
  password: {
    type: String,
    required: [true, "Veuillez choisir un mot de passe"]
  },
});

//Plugin pour garantir un email unique
userSchema.plugin(uniqueValidator);

module.exports = mongoose.model("User", userSchema);