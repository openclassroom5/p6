const express = require("express");
const router = express.Router();

const auth = require("../middleware/auth");
const multer = require("../middleware/multer");
const saucesCtrl = require("../controllers/sauce");

// --- GET ---
router.get("/", auth, saucesCtrl.getAllSauces);
router.get("/:id", auth, saucesCtrl.getOneSauce);

// --- POST ---
router.post("/", auth, multer, saucesCtrl.createSauce);
router.post("/:id/like", auth, saucesCtrl.likeSauce);

// --- PUT ---
router.put("/:id", auth, multer, saucesCtrl.modifySauce);

// --- DELETE ---
router.delete("/:id", auth, saucesCtrl.deleteSauce);





// route for likes


module.exports = router;