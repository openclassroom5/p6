const express = require("express");
const router = express.Router();

const userCtrl = require('../controllers/user');
const passwordCheck = require("../middleware/password");

// --- POST : SIGNUP + LOGIN ---
router.post('/signup', userCtrl.signup);
router.post('/login', userCtrl.login);

module.exports = router;